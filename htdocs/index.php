<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Assignment 01 - Border, Ethan</title>
	</head>
	<body>
		<h1><center>Assignment 01 - Border, Ethan<center></h1>

		<div>
			<h2>Summary</h2>

			<p>My name is Ethan Border. I was born on February 26, 2001. I grew up in Willoughby, Ohio. I attended South High School, and then went to Bowling Green State University majoring in Computer Science.</p>
		</div>

		<div>
			<h2>Personal Information</h2>

			<p>My name is Ethan Border. My address is 1612 Carp Street. My phone number is +1(403)-151-1951. My email address is eborder@bgsu.edu</p>
		</div>

		<div>
			<h2>Academic Information</h2>

			<ul>
				<li>South High School
					<ul>
						<li>English and Writing</li>
						<li>Mathematics</li>
						<li>History and Government</li>
						<li>Spanish</li>
						<li>Biology, Chemistry, and Physics</li>
						<li>Art</li>
					</ul>
				</li>
				<li>Bowling Green State University
					<ul>
						<li>Computer Sciences</li>
						<li>Discrete Math, Calculus and Statistics</li>
						<li>Spanish</li>
						<li>Pop Culture, Ethnic Studies, and Philosophy</li>
						<li>Biology</li>
					</ul>
				</li>
			</ul>
		</div>

		<div>
			<h2>Employment Information</h2>

			<ul>
				<li>Past
					<ul>
						<li>Eat 'n Park</li>
						<li>Arby's</li>
					</ul>
				</li>
				<li>Present
					<ul>
						<li>Target</li>
					</ul>
				</li>
				<li>Future
					<ul>
						<li>CS Internship</li>
					</ul>
				</li>
			</ul>
		</div>
	</body>
</html>